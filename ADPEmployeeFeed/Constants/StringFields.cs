﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADPEmployeeFeed.Constants
{
    public static class StringFields
    {
        public static class ItemIds
        {
            public const string TrendId = "1586553128832665_85";
            public const string FavoriteFood = "9200093947219_1";
            public const string FavoriteSportsTeam = "9200093947263_1";
            public const string ALittleBitAboutMe = "9200093947386_1";
            public const string FavoriteHobby = "9200093947164_1";
        }

        public static class ItemTypes
        {
            public const string Education = "Education";
            public const string Membership = "Membership";
            public const string Certification = "Certification";
            public const string Competency = "Competency";
            public const string BusinessUnit = "Business Unit";
            public const string Department = "Department";
        }
    }
}
