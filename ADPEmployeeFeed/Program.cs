﻿using ADPClient;
using ADPClient.ADPException;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using ADPEmployeeFeed.models;
using NLog;
using System.Net.Mail;
using ADPEmployeeFeed.Constants;
using ADPEmployeeFeed.Models;
using System.Collections.Generic;
using System.Reflection;

namespace ADPEmployeeFeed
{
    internal class Program
    {
        public static void Main()
        {
            var logger = LogManager.GetLogger("file");
            logger.Info("Starting the ADP Employee Feed Background Job ...");
            LoadEmployeeData(logger);
            //LoadOneEmployeeData(logger);
            logger.Info("ADP Employee Feed Job Ends");
        }

        public static void LoadEmployeeData(Logger logger)
        {
            var clientConfig = GetEmbeddedConfig("Content.config.default.json");
            var connectionCfg = JSONUtil.Deserialize<ClientCredentialConfiguration>(clientConfig);
            Console.WriteLine(clientConfig);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var connection = (ClientCredentialConnection)ADPApiConnectionFactory.createConnection(connectionCfg);
            
            try
            {
                try
                {
                    connection.connect();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                    logger.Error(ServicePointManager.SecurityProtocol);
                    while (e != null)
                    {
                        logger.Error(e, $"Failed to connect: {e.Message}");
                        e = e.InnerException;
                    }

                    Environment.ExitCode = 2;
                }

                Console.WriteLine(ServicePointManager.SecurityProtocol);
                Console.WriteLine("Connection status:  " + connection.isConnectedIndicator());
                if (connection.isConnectedIndicator())
                {
                    //token = connection.accessToken;
                    var top = 100;
                    var skip = 0;
                    var jobRequisitionTop = 20;
                    var jobRequisitionSkip = 0;
                    // this condition really required? $filter=workers/workAssignments/assignmentStatus/statusCode/codeValue eq 'A'
                    const string baseUrl = "/hr/v2/workers?&$top=";
                    const string baseUrlTalent = "/talent/v2/associates/";
                    const string baseUrlJobRquisitions = "/staffing/v1";
                    const string urlEducationalDegree = "/associate-educational-degrees";
                    const string urlMembership = "/associate-memberships";
                    const string urlCertification = "/associate-certifications";
                    const string urlCompetency = "/associate-competencies";
                    const string urlJobRequisitions = "/job-requisitions?&$top=";

                    var today = DateTime.Now.DayOfWeek;

                    for (var i = 0; i < 12; i++)
                    {
                        var queryUrl = baseUrl + top + "&$skip= " + skip;
                        var baseFeed = connection.getADPData(queryUrl);
                        var feed = JsonConvert.DeserializeObject<RootObject>(baseFeed);

                        if (feed?.workers?.Count > 0)
                        {
                            using (var db = new APIEntities())
                            {
                                foreach (var worker in feed.workers)
                                {
                                    // find record
                                    var assignment = GetPrimaryWorkAssignment(worker);
                                    var workerDb = db.ADPEmployeeFeed.FirstOrDefault(x => x.AssociateId == worker.associateOID);

                                    if (workerDb != null)
                                    {
                                        //Update Talent
                                        if (today == DayOfWeek.Thursday)
                                        {
                                            AddCompetency(connection, db, baseUrlTalent, worker, urlCompetency);
                                        }

                                        if (today == DayOfWeek.Friday)
                                        {
                                            AddCertification(connection, db, baseUrlTalent, worker, urlCertification);
                                        }

                                        if (today == DayOfWeek.Saturday)
                                        {
                                            AddMembership(connection, db, baseUrlTalent, worker, urlMembership);
                                        }

                                        if (today == DayOfWeek.Sunday)
                                        {
                                            AddEducation(connection, db, baseUrlTalent, worker, urlEducationalDegree);
                                        }

                                        // update Record
                                        workerDb.FirstName = worker.person?.legalName?.givenName;
                                        workerDb.LastName = worker.person?.legalName?.familyName1;
                                        workerDb.MiddleName = worker.person?.legalName?.middleName;
                                        workerDb.FormattedName = worker.person?.legalName?.formattedName;
                                        logger.Debug("Update - AssocId::" + worker.associateOID + ", Email::" + GetEmail(worker));
                                        workerDb.Email = GetEmail(worker);
                                        workerDb.TrendId = GetTrendId(worker);
                                        workerDb.ContactNumber = GetContactNumber(worker);
                                        workerDb.PreferedName = worker.person?.preferredName?.givenName;
                                        workerDb.PositionStatus = worker.workerStatus?.statusCode?.codeValue;
                                        workerDb.HireDate = Convert.ToDateTime(worker.workerDates.originalHireDate);
                                        workerDb.JobTitle = assignment?.jobTitle;
                                        workerDb.Location = GetLocation(assignment);
                                        workerDb.WorkerTypeCode = assignment?.workerTypeCode?.codeValue;
                                        workerDb.WorkerType = assignment?.workerTypeCode?.shortName;
                                        workerDb.ReportsToId1 = assignment?.reportsTo?[0].associateOID;
                                        workerDb.ReportsToName1 = assignment?.reportsTo?[0].reportsToWorkerName?.formattedName;
                                        workerDb.IsMgmt = assignment?.managementPositionIndicator;
                                        workerDb.PositionId = assignment?.positionID;
                                        workerDb.PayrollCompanyCode = assignment?.payrollGroupCode;
                                        workerDb.UpdatedDate = DateTime.Now;
                                        workerDb.FavoriteHobby = GetPersonalInfo(worker, Constants.StringFields.ItemIds.FavoriteHobby);
                                        workerDb.FavoriteFood = GetPersonalInfo(worker, Constants.StringFields.ItemIds.FavoriteFood);
                                        workerDb.FavoriteSportsTeam = GetPersonalInfo(worker, Constants.StringFields.ItemIds.FavoriteSportsTeam);
                                        workerDb.ALittleBitAboutMe = GetPersonalInfo(worker, Constants.StringFields.ItemIds.ALittleBitAboutMe);
                                        //workerDb.Race = worker?.person?.raceCode?.longName;
                                        //workerDb.Ethnicity = worker?.person?.ethnicityCode?.longName;
                                        //workerDb.Gender = worker?.person?.genderCode?.longName;

                                        FillInWorkDetails(worker, workerDb);

                                        if (workerDb.ReportsToId1 != null)
                                        {
                                            continue;
                                        }

                                        for (var j = 1; j < 5; j++)
                                        {
                                            if (workerDb.ReportsToId1 != null ||
                                                !(worker.workAssignments?.Count >= j + 1) ||
                                                worker.workAssignments[j].reportsTo == null)
                                            {
                                                continue;
                                            }
                                            workerDb.ReportsToId1 = worker.workAssignments?[j].reportsTo?[0].associateOID;
                                            workerDb.ReportsToName1 = worker.workAssignments?[j].reportsTo?[0].reportsToWorkerName?.formattedName;
                                            break;
                                        }
                                    }
                                    else
                                    {

                                        AddCompetency(connection, db, baseUrlTalent, worker, urlCompetency);

                                        AddCertification(connection, db, baseUrlTalent, worker, urlCertification);

                                        AddMembership(connection, db, baseUrlTalent, worker, urlMembership);

                                        AddEducation(connection, db, baseUrlTalent, worker, urlEducationalDegree);

                                        // Save new record
                                        var emp = new ADPEmployeeFeed
                                        {
                                            AssociateId = worker.associateOID,
                                            FirstName = worker.person?.legalName?.givenName,
                                            LastName = worker.person?.legalName?.familyName1,
                                            PreferedName = worker.person?.preferredName?.givenName,
                                            MiddleName = worker.person?.legalName?.middleName,
                                            FormattedName = worker.person?.legalName?.formattedName,
                                            Email = GetEmail(worker),
                                            TrendId = GetTrendId(worker),
                                            ContactNumber = GetContactNumber(worker),
                                            PositionStatus = worker.workerStatus?.statusCode?.codeValue,
                                            HireDate = Convert.ToDateTime(worker.workerDates.originalHireDate),
                                            JobTitle = assignment?.jobTitle,
                                            Location = GetLocation(assignment),
                                            WorkerTypeCode = assignment?.workerTypeCode?.codeValue,
                                            WorkerType = assignment?.workerTypeCode?.shortName,
                                            ReportsToId1 = assignment?.reportsTo?[0].associateOID,
                                            ReportsToName1 = assignment?.reportsTo?[0].reportsToWorkerName?.formattedName,
                                            IsMgmt = assignment?.managementPositionIndicator,
                                            PositionId = assignment?.positionID,
                                            PayrollCompanyCode = assignment?.payrollGroupCode,
                                            CreatedDate = DateTime.Now,
                                            FavoriteFood = GetPersonalInfo(worker, Constants.StringFields.ItemIds.FavoriteFood),
                                            FavoriteSportsTeam = GetPersonalInfo(worker, Constants.StringFields.ItemIds.FavoriteSportsTeam),
                                            ALittleBitAboutMe = GetPersonalInfo(worker, Constants.StringFields.ItemIds.ALittleBitAboutMe),
                                            //Gender = worker?.person?.genderCode?.longName,
                                            //Race = worker?.person?.raceCode?.longName,
                                            //Ethnicity = worker?.person?.ethnicityCode?.longName
                                        };

                                        FillInWorkDetails(worker, emp);

                                        if (emp.ReportsToId1 == null)
                                        {
                                            for (var j = 1; j < 5; j++)
                                            {
                                                if (emp.ReportsToId1 != null ||
                                                    !(worker.workAssignments?.Count >= j + 1) ||
                                                    worker.workAssignments[j].reportsTo == null)
                                                {
                                                    continue;
                                                }
                                                emp.ReportsToId1 = worker.workAssignments?[j].reportsTo?[0].associateOID;
                                                emp.ReportsToName1 = worker.workAssignments?[j].reportsTo?[0].reportsToWorkerName?.formattedName;
                                                break;
                                            }
                                        }

                                        db.ADPEmployeeFeed.Add(emp);
                                    }
                                }
                                db.SaveChanges();
                            }
                            top += 100;
                            skip += 100;
                        }
                        else
                        {
                            break;
                        }
                    }
                    using (APIEntities db = new APIEntities())
                    {
                        db.UpdateADPEmployeeFeed();
                    }

                    using (var db = new APIEntities())
                    {
                        db.ADPJobRequisitionsFeed.RemoveRange(db.ADPJobRequisitionsFeed);
                        db.SaveChanges();
                    }

                    for (var i = 0; i < 12; i++)
                    {
                        var queryUrlJobRequisitions = urlJobRequisitions + jobRequisitionTop + "&$skip= " + jobRequisitionSkip + "&$filter=visibleToJobSeekerIndicator%20eq%20true";

                        AddJobRequisitions(connection, baseUrlJobRquisitions, queryUrlJobRequisitions);
                        jobRequisitionTop += 20;
                        jobRequisitionSkip += 20;
                    }
                }
            }
            catch (ADPConnectionException e)
            {
                logger.Error(e, $"ADP Conection Error: {e.Message}");
                Environment.ExitCode = 3;
                SendEmail(e.ToString());
            }
            catch (Exception e)
            {
                logger.Error(e, $"General Block Error: {e.Message}");
                Environment.ExitCode = 4;
                SendEmail(e.ToString());
            }
        }

        private static void AddJobRequisitions(ClientCredentialConnection connection, string baseUrlJobRquisitions, string urlJobRequisitions)
        {
            using (var db = new APIEntities())
            {
                var queryUrlJobRequisitions = baseUrlJobRquisitions + urlJobRequisitions;
                var baseFeedJobRequisitions = connection.getADPData(queryUrlJobRequisitions);
                var feedJobRequisitions = JsonConvert.DeserializeObject<JobRequisitionsObject>(baseFeedJobRequisitions);

                if (feedJobRequisitions?.jobRequisitions.Count > 0)
                {
                    foreach (var jobRequisitionObject in feedJobRequisitions.jobRequisitions)
                    {
                        var jobRequisitionDb = db.ADPJobRequisitionsFeed.FirstOrDefault(x => x.ItemId == jobRequisitionObject.itemID);

                        if (jobRequisitionDb == null)
                        {
                            var jobRequisition = new ADPJobRequisitionsFeed()
                            {
                                ItemId = jobRequisitionObject.itemID,
                                JobPosition = jobRequisitionObject.postingInstructions?.FirstOrDefault()?.nameCode?.codeValue,
                                Posting = jobRequisitionObject.postingInstructions?.FirstOrDefault()?.nameCode?.longName,
                                City = jobRequisitionObject.requisitionLocations?.FirstOrDefault()?.address?.cityName,
                                State = jobRequisitionObject.requisitionLocations?.FirstOrDefault()?.address?.countrySubdivisionLevel1?.codeValue,
                                District = GetOrganizationalUnits(jobRequisitionObject.organizationalUnits, StringFields.ItemTypes.BusinessUnit),
                                Department = GetOrganizationalUnits(jobRequisitionObject.organizationalUnits, StringFields.ItemTypes.Department),
                                HiringManagerId = jobRequisitionObject.hiringManager?.associateOID,
                                EffectiveDate = jobRequisitionObject.requisitionStatusCode?.effectiveDate,
                                ProjectedStartDate = jobRequisitionObject.projectedStartDate,
                                CreateDate = DateTime.Now.ToString("MM/dd/yyyy"),
                                InternalIndicator = jobRequisitionObject.InternalIndicator,
                                VisibleToJobSeekerIndicator = jobRequisitionObject.VisibleToJobSeekerIndicator,
                                Status = jobRequisitionObject.requisitionStatusCode?.shortName
                            };

                            db.ADPJobRequisitionsFeed.Add(jobRequisition);
                            db.SaveChanges();
                        }
                    }
                }
            }
        }

        private static string GetOrganizationalUnits(List<OrganizationalUnits> organizationalUnits, string typeCode)
        {
            var shortName = string.Empty;
            
            if (organizationalUnits != null)
            {
                foreach (var organizationUnit in organizationalUnits)
                {
                    if (organizationUnit.typeCode.shortName == typeCode)
                    {
                        shortName = organizationUnit.nameCode?.shortName;
                    }
                }
            }

            return shortName;
        }

        private static void AddEducation(ClientCredentialConnection connection, APIEntities db, string baseUrlTalent, Worker worker, string urlEducationalDegree)
        {
            var queryUrlEducation = baseUrlTalent + worker.associateOID + urlEducationalDegree;
            var baseFeedEducation = connection.getADPData(queryUrlEducation);
            var feedEducation = JsonConvert.DeserializeObject<Talent>(baseFeedEducation);

            if (feedEducation?.associateEducationalDegrees.Count > 0)
            {
                foreach (var associateEducation in feedEducation.associateEducationalDegrees)
                {
                    var talentDb = db.ADPTalentFeed.FirstOrDefault(x => x.ItemId == associateEducation.itemID);

                    if (talentDb == null)
                    {
                        var talent = new ADPTalentFeed
                        {
                            ItemId = associateEducation.itemID,
                            AssociateId = worker.associateOID,
                            EducationDescription = associateEducation.educationDescription,
                            EducationalInstitution = associateEducation?.educationalInstitutionAttendances?.FirstOrDefault()?.educationalInstitution?.nameCode?.longName,
                            Type = StringFields.ItemTypes.Education
                        };

                        db.ADPTalentFeed.Add(talent);
                        db.SaveChanges();
                    }
                }
            }
        }

        private static void AddMembership(ClientCredentialConnection connection, APIEntities db, string baseUrlTalent, Worker worker, string urlMembership)
        {
            var queryUrlMembership = baseUrlTalent + worker.associateOID + urlMembership;
            var baseFeedMembership = connection.getADPData(queryUrlMembership);
            var feedMembership = JsonConvert.DeserializeObject<Talent>(baseFeedMembership);

            if (feedMembership?.associateMemberships.Count > 0)
            {
                foreach (var associateMembership in feedMembership.associateMemberships)
                {
                    var talentDb = db.ADPTalentFeed.FirstOrDefault(x => x.ItemId == associateMembership.itemID);

                    if (talentDb == null)
                    {
                        var talent = new ADPTalentFeed
                        {
                            ItemId = associateMembership.itemID,
                            AssociateId = worker.associateOID,
                            MembershipOrganization = associateMembership?.membershipOrganization?.nameCode?.longName,
                            Type = StringFields.ItemTypes.Membership
                        };

                        db.ADPTalentFeed.Add(talent);
                        db.SaveChanges();
                    }
                }
            }
        }

        private static void AddCertification(ClientCredentialConnection connection, APIEntities db, string baseUrlTalent, Worker worker, string urlCertification)
        {
            var queryUrlCertification = baseUrlTalent + worker.associateOID + urlCertification;
            var baseFeedCertification = connection.getADPData(queryUrlCertification);
            var feedCertification = JsonConvert.DeserializeObject<Talent>(baseFeedCertification);

            if (feedCertification?.associateCertifications.Count > 0)
            {
                foreach (var associateCertification in feedCertification.associateCertifications)
                {
                    var talentDb = db.ADPTalentFeed.FirstOrDefault(x => x.ItemId == associateCertification.itemID);

                    if (talentDb == null)
                    {
                        var talent = new ADPTalentFeed
                        {
                            ItemId = associateCertification.itemID,
                            AssociateId = worker.associateOID,
                            CertificationNameCode = associateCertification?.certificationNameCode?.longName,
                            Type = StringFields.ItemTypes.Certification
                        };

                        db.ADPTalentFeed.Add(talent);
                        db.SaveChanges();
                    }
                }
            }
        }

        private static void AddCompetency(ClientCredentialConnection connection, APIEntities db, string baseUrlTalent, Worker worker, string urlCompetency)
        {
            var queryUrlCompetency = baseUrlTalent + worker.associateOID + urlCompetency;
            var baseFeedCompetency = connection.getADPData(queryUrlCompetency);
            var feedCompetency = JsonConvert.DeserializeObject<Talent>(baseFeedCompetency);

            if (feedCompetency?.associateCompetencies.Count > 0)
            {
                foreach (var associateCompetency in feedCompetency.associateCompetencies)
                {
                    var talentDb = db.ADPTalentFeed.FirstOrDefault(x => x.ItemId == associateCompetency.itemID);

                    if (talentDb == null)
                    {
                        var talent = new ADPTalentFeed
                        {
                            ItemId = associateCompetency.itemID,
                            AssociateId = worker.associateOID,
                            CompetencyNameCode = associateCompetency?.competencyNameCode?.longName,
                            CategoryCode = associateCompetency?.categoryCode?.longName,
                            SelfAssessedProficiencyScore = associateCompetency?.selfAssessedProficiencyScore?.scoreCode?.longName,
                            Type = StringFields.ItemTypes.Competency
                        };

                        db.ADPTalentFeed.Add(talent);
                        db.SaveChanges();
                    }
                }
            }
        }

        public static string GetEmail(Worker worker)
        {
            if (worker.businessCommunication != null && worker.businessCommunication.emails != null &&
                worker.businessCommunication.emails.Count > 0)
            {
                return worker.businessCommunication.emails[0].emailUri;
            }
            // Employee with no email is not a valid scenario, so filling with dummy
            return "noemail@mingledorffs.com";
        }

        public static string GetContactNumber(Worker worker)
        {
            if (worker.businessCommunication != null && worker.businessCommunication.mobiles != null &&
                worker.businessCommunication.mobiles.Count > 0)
            {
                return (worker.businessCommunication.mobiles[0].areaDialing +
                        worker.businessCommunication.mobiles[0].dialNumber);
            }
            return "";
        }

        public static WorkAssignment GetPrimaryWorkAssignment(Worker worker)
        {
            foreach (var assign in worker.workAssignments.Where(assign => assign.primaryIndicator))
            {
                return assign;
            }
           
            return worker.workAssignments[0];
        }

        public static string GetLocation(WorkAssignment assignment)
        {
            var loc = assignment?.homeWorkLocation?.nameCode.shortName;
            if (string.IsNullOrEmpty(loc))
            {
                loc = assignment?.homeWorkLocation?.nameCode.longName;
            }

            return loc;
        }

        public static string GetTrendId(Worker worker)
        {
            if (worker.customFieldGroup != null && worker.customFieldGroup.stringFields != null &&
                worker.customFieldGroup.stringFields.Count > 0)
            {
                foreach (var stringField in worker.customFieldGroup.stringFields)
                {
                    if (stringField.itemId.Equals(Constants.StringFields.ItemIds.TrendId))
                    {
                        if (stringField.stringValue != null)
                        {
                            return stringField.stringValue.ToUpper();
                        }
                        return stringField.stringValue;
                    }
                }
            }
            return null;
        }

        public static string GetPersonalInfo(Worker worker, string personalInfo)
        {
            if (worker.person.customFieldGroup?.stringFields != null && worker.person.customFieldGroup.stringFields.Count > 0)
            {
                foreach (var stringField in worker.person.customFieldGroup.stringFields)
                {
                    if (stringField.itemId.Equals(personalInfo))
                    {
                        return stringField.stringValue;
                    }
                }
            }
            return null;
        }

        public static void FillInWorkDetails(Worker worker, ADPEmployeeFeed workerDb)
        {
            var assignment = GetPrimaryWorkAssignment(worker);
            if (assignment?.homeOrganizationalUnits == null ||
                assignment.homeOrganizationalUnits.Count <= 0) return;

            foreach (var org in assignment.homeOrganizationalUnits)
            {
                if (org.typeCode.codeValue.Equals("Business Unit"))
                {
                    workerDb.BusinessUnit = org.nameCode.longName ?? org.nameCode.shortName;
                }
                if (org.typeCode.codeValue.Equals("Department"))
                {
                    workerDb.HomeDept = org.nameCode.longName ?? org.nameCode.shortName;
                    workerDb.HomeDeptCode = org.nameCode.codeValue;
                }
                if (assignment.terminationDate != null)
                {
                    workerDb.TerminationDate = Convert.ToDateTime(assignment.terminationDate);
                }
                else
                {
                    workerDb.TerminationDate = null;
                }
            }

            workerDb.StatusCode = assignment.assignmentStatus?.statusCode?.codeValue;

        }

        public static void SendEmail(string errorMessage)
        {
            try
            {
                MailMessage message = new MailMessage();
                SmtpClient smtp = new SmtpClient();
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"]);
                message.To.Add(new MailAddress(ConfigurationManager.AppSettings["AdminEmail"]));
                message.Subject = "ADPEmployeeFeed Nightly Job Executed with Error(s)";
                message.IsBodyHtml = true; //to make message body as html  
                message.Body = "Error:  " + errorMessage;
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["EmailPort"]);
                smtp.Host = ConfigurationManager.AppSettings["EmailServer"];
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailUser"], ConfigurationManager.AppSettings["EmailPassword"]);
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                var logger = LogManager.GetLogger("file");
                // ignored
                while (ex != null)
                {
                    logger.Error(ex, $"Send Email Failed: {ex.Message}");
                    ex = ex.InnerException;
                }

            }
        }

        private static string GetEmbeddedConfig(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var name = $"{assembly.GetName().Name}.{fileName}";
            using (Stream stream = assembly.GetManifestResourceStream(name))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
