﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADPEmployeeFeed
{
    class Employee
    {
        public string AssociateId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PreferedName { get; set; }
        public string PositionStatus { get; set; }
        public DateTime HireDate { get; set; }
        public string PayrollCompanyCode { get; set; }
        public string JobTitle { get; set; }
        public string Location { get; set; }
        public string HomeDept { get; set; }
        public string Email { get; set; }
        public string ReportsToId { get; set; }
        public string ReportsToName { get; set; }
        public string ReportsToEmail { get; set; }
        public bool IsMgmt { get; set; }
        public string PositionId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
