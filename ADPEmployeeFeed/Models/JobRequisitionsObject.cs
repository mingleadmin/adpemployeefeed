﻿using System.Collections.Generic;
using ADPEmployeeFeed.models;

namespace ADPEmployeeFeed.Models
{
    public class JobRequisitionsObject
    {
        public List<JobRequisitions> jobRequisitions { get; set; }
    }

    public class HiringManager
    {
        public string associateOID { get; set; }
    }

    public class RequisitionStatusCode
    {
        public string effectiveDate { get; set; }
        public string shortName { get; set; }
        public string codeValue { get; set; }
    }

    public class OrganizationalUnits
    {
        public TypeCode typeCode { get; set; }
        public NameCode nameCode { get; set; }
    }

    public class RequisitionLocations
    {
        public Address address { get; set; }
    }

    public class Address
    {
        public string cityName { get; set; }
        public CountrySubdivisionLevel1 countrySubdivisionLevel1 { get; set; }
    }

    public class CountrySubdivisionLevel1
    {
        public string codeValue { get; set; }
    }

    public class JobRequisitions
    {
        public string itemID { get; set; }
        public List<PostingInstructions> postingInstructions { get; set; }
        public List<RequisitionLocations> requisitionLocations { get; set; }
        public List<OrganizationalUnits> organizationalUnits { get; set; }
        public RequisitionStatusCode requisitionStatusCode { get; set; }
        public HiringManager hiringManager { get; set; }
        public string projectedStartDate { get; set; }
        public string InternalIndicator { get; set; }
        public string VisibleToJobSeekerIndicator { get; set; }
    }

    public class PostingInstructions
    {
        public NameCode nameCode { get; set; }
    }

    
}
