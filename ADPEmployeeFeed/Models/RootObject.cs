﻿using System.Collections.Generic;

namespace ADPEmployeeFeed.models
{

    public class PreferredName
    {
        public string givenName { get; set; }
    }

    public class LegalName
    {
        public string givenName { get; set; }
        public string familyName1 { get; set; }
        public string middleName { get; set; }
        public string formattedName { get; set; }
    }

    public class Person
    {
        public PreferredName preferredName { get; set; }
        public LegalName legalName { get; set; }
        public CustomFieldGroup customFieldGroup { get; set; }
        public GenderCode genderCode { get; set; }
        public EthnicityCode ethnicityCode { get; set; }
        public RaceCode raceCode { get; set; }
    }

    public class RaceCode
    {
        public string shortName { get; set; }
        public string longName { get; set; }
    }

    public class EthnicityCode
    {
        public string shortName { get; set; }
        public string longName { get; set; }
    }

    public class GenderCode
    {
        public string shortName { get; set; }
        public string longName { get; set; }
    }

    public class WorkerDates
    {
        public string originalHireDate { get; set; }
    }

    public class StatusCode
    {
        public string codeValue { get; set; }
    }

    public class WorkerStatus
    {
        public StatusCode statusCode { get; set; }
    }


    public class Email2
    {
        public string emailUri { get; set; }
    }

    public class Mobile
    {
        public string itemID { get; set; }
        public string areaDialing { get; set; }
        public string dialNumber { get; set; }
    }

    public class BusinessCommunication
    {
        public List<Email2> emails { get; set; }
        public List<Mobile> mobiles { get; set; }
    }
    public class CustomFieldGroup
    {
        public List<StringField> stringFields { get; set; }
    }

    public class StringField
    {
        public string itemId { get; set; }
        public string stringValue { get; set; }

    }

    public class NameCode13
    {
        public string codeValue { get; set; }
        public string longName { get; set; }
        public string shortName { get; set; }
    }

    public class TypeCode
    {
        public string codeValue { get; set; }
        public string shortName { get; set; }
    }

    public class HomeOrganizationalUnit
    {
        public NameCode13 nameCode { get; set; }
        public TypeCode typeCode { get; set; }
    }

    public class NameCode14
    {
        public string codeValue { get; set; }
        public string longName { get; set; }
        public string shortName { get; set; }
    }

    public class TypeCode2
    {
        public string codeValue { get; set; }
        public string shortName { get; set; }
    }

    public class AssignedOrganizationalUnit
    {
        public NameCode14 nameCode { get; set; }
        public TypeCode2 typeCode { get; set; }
    }

    public class ReportsToWorkerName
    {
        public string formattedName { get; set; }
    }

    public class ReportsTo
    {
        public string associateOID { get; set; }
        public ReportsToWorkerName reportsToWorkerName { get; set; }
    }

    public class NameCode17
    {
        public string longName { get; set; }
    }

    public class Address2
    {
        public NameCode17 nameCode { get; set; }
    }

    public class HomeWorkLocation
    {
        public Address2 address { get; set; }
        public NameCode13 nameCode { get; set; }
    }

    public class WorkAssignment
    {
        public string hireDate { get; set; }
        public string jobTitle { get; set; }
        public string positionID { get; set; }
        public List<HomeOrganizationalUnit> homeOrganizationalUnits { get; set; }
        public List<AssignedOrganizationalUnit> assignedOrganizationalUnits { get; set; }
        public List<ReportsTo> reportsTo { get; set; }
        public string payrollGroupCode { get; set; }
        public bool managementPositionIndicator { get; set; }
        public HomeWorkLocation homeWorkLocation { get; set; }
        public TypeCode workerTypeCode { get; set; }
        public AssignmentStatus assignmentStatus { get; set; }
        public bool primaryIndicator { get; set; }
        public string terminationDate { get; set; }

    }

    public class AssignmentStatus
    {
        public NameCode13 statusCode { get; set; }
    }

    public class Worker
    {
        public string associateOID { get; set; }
        public Person person { get; set; }
        public WorkerDates workerDates { get; set; }
        public WorkerStatus workerStatus { get; set; }
        public BusinessCommunication businessCommunication { get; set; }
        public List<WorkAssignment> workAssignments { get; set; }
        public CustomFieldGroup customFieldGroup { get; set; }
    }

    public class RootObject
    {
        public List<Worker> workers { get; set; }
    }

    public class NameCode
    {
        public string codeValue { get; set; }
        public string shortName { get; set; }
        public string longName { get; set; }
    }
}
