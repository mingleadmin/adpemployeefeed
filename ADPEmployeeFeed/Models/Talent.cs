﻿using ADPEmployeeFeed.models;
using System.Collections.Generic;

namespace ADPEmployeeFeed.Models
{
    public class Talent
    {
        public List<AssociateEducationalDegree> associateEducationalDegrees { get; set; }
        public List<AssociateMemberships> associateMemberships { get; set; }
        public List<AssociateCertifications> associateCertifications { get; set; }
        public List<AssociateCompetencies> associateCompetencies { get; set; }
    }

    public class AssociateCompetencies
    {
        public string itemID { get; set; }
        public CompetencyNameCode competencyNameCode { get; set; }
        public CategoryCode categoryCode { get; set; }
        public SelfAssessedProficiencyScore selfAssessedProficiencyScore { get; set; }
    }

    public class CompetencyNameCode
    {
        public string codeValue { get; set; }
        public string longName { get; set; }
    }

    public class CategoryCode
    {
        public string codeValue { get; set; }
        public string shortName { get; set; }
        public string longName { get; set; }
    }

    public class SelfAssessedProficiencyScore
    {
        public ScoreCode scoreCode { get; set; }
    }

    public class ScoreCode
    {
        public string codeValue { get; set; }
        public string shortName { get; set; }
        public string longName { get; set; }
    }

    public class AssociateCertifications
    {
        public string itemID { get; set; }
        public CertificationNameCode certificationNameCode { get; set; }
    }

    public class CertificationNameCode
    {
        public string codeValue { get; set; }
        public string longName { get; set; }
    }

    public class AssociateMemberships
    {
        public string itemID { get; set; }
        public MembershipOrganization membershipOrganization { get; set; }
    }

    public class MembershipOrganization
    {
        public NameCode nameCode { get; set; }
    }


    public class AssociateEducationalDegree
    {
        public string itemID { get; set; }
        public List<EducationalInstitutionAttendances> educationalInstitutionAttendances { get; set; }

        public string educationDescription { get; set; }
    }

    public class EducationalInstitutionAttendances
    {
        public EducationalInstitution educationalInstitution { get; set; }
    }

    public class EducationalInstitution
    {
        public NameCode nameCode { get; set; }
    }
}
